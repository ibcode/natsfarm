package bjm.test;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;

/**
 * Created by Benji on 11/01/14.
 */
public class PresentBlock extends Block {

    public PresentBlock(int par1, Material par2Material){
        super(par1, par2Material);
    }

    @Override
    public void registerIcons(IconRegister reg){
        this.blockIcon = reg.registerIcon("test:test_block");
    }


}
