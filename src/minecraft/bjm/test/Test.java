package bjm.test;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLLoadEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.MinecraftForge;

/**
 * Created by Benji on 06/01/14.
 */

@Mod(modid = "Test1", name = "Test1", version = "0.1")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class Test {
    // Block section
    public final static Block PresentBlock =
            new PresentBlock(501, Material.rock)
    .setHardness(2.0F)
    .setStepSound(Block.soundStoneFootstep)
    .setUnlocalizedName("testBlock")
    .setCreativeTab(CreativeTabs.tabBlock);

    // Instance of our Mod

    public static Test instance;

    String message = "";

    @EventHandler
    public void preInit(FMLPreInitializationEvent event){
        // Test Block Stuff
        LanguageRegistry.addName(PresentBlock, "Present");
        MinecraftForge.setBlockHarvestLevel(PresentBlock, "pickaxe", 2);
        GameRegistry.registerBlock(PresentBlock, "PresentBlock");
    }

    @EventHandler
    public void load(FMLLoadEvent event){

    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event){

    }

    @EventHandler
    public void ServerStart(FMLServerStartingEvent event){

    }
}
