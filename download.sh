#!/bin/sh
cd ~ # Move to home directory
mkdir MCModding # Make a directory for Minecraft modding...
cd MCModding # ...move into it
curl -o forgedownload.zip http://files.minecraftforge.net/maven/net/minecraftforge/forge/1.6.4-9.11.1.965/forge-1.6.4-9.11.1.965-src.zip # Grab forge
unzip forgedownload.zip -d RENAME_ME # Unzip it into a directory to be renamed by user
cd RENAME_ME # Move into directory
cd forge # Move into forge
chmod +x install.sh
./install.sh # Run forge script
cd .. # Move to RENAME-ME
mkdir tempDir # Make temporary directory
cd forge/mcp/ # Move to the MCP directory within the Forge directory
#mv jars/ ../../tempDir/ # Get the jars folder out the way before we break it
mv src/ ../../tempDir/ # Same for the src folder
rm LICENSE.txt # Use the server's version, identical
git init # Make a git directory here
git remote add origin https://bitbucket.org/ibcode/natsfarm.git # Assign the remote
# Alternatively (comment out above line and use this):
# git remote add origin git@bitbucket.org/ibcode/modbox.git
git pull origin master # Download all of the files from the repo
# Sort out jars directory
cd ../../tempDir/ # Move to temporary directory
#mv jars/config ./ # Get the config folder out of the way
#mv jars/* ../forge/mcp/jars/ # Put the none-offending files into the forge/mcp/jars directory
#rmdir jars # Sorted that out
#cp config/* ../forge/mcp/jars/config/ # Put all of the other config files in
#rmdir config # Done all of the jars stuff, so clean up after ourselves
# Sort out the src directory
mv src/minecraft/assets ./ # Get the offending assets folder out of the way
mv src/minecraft/* ../forge/mcp/src/minecraft/ # The rest won't conflict, so dump that straight in
rmdir src/minecraft # Finished with the src directory, so get rid of its contents...
rmdir src           # ...and the directory itself
mv assets/* ../forge/mcp/src/minecraft/assets/ # Move all of the assets stuff into the folder
rmdir assets # Done with the assets folder, so get rid of it
cd .. # Move to the RENAME_ME directory
rmdir tempDir # Finished with so clean up
# Done! 
