# A basic mod

## Install

```
#!sh
curl https://bitbucket.org/ibcode/natsfarm/raw/d221de20d4ceca09bd0514f042a3a253b5298ac0/download.sh | sh
```

**OR**

To install; download [this](https://bitbucket.org/ibcode/modbox/raw/562fef33e6dcb03960f9f67ec72c92923d612eb8/download.sh) on nix by copy and pasting this into a shell file, and running it after `chmod`'ing it. Make sure you have astyle (`sudo apt-get install astyle`) installed. No Windows version as of yet.